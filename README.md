# IP Validation View
Latest Version: [![](https://jitpack.io/v/com.gitlab.technowolf/ip-validation-view.svg)](https://jitpack.io/#com.gitlab.technowolf/ip-validation-view)
## Setup

Add it in your root `build.gradle` at the end of repositories:

```groovy
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}
```

Add the dependency

```groovy
dependencies {
   implementation "com.gitlab.technowolf:ip-validation-view:$version"
}
```

## Usage
Demo implementation [here](app/src/main/java/in/technowolf/ipvalidationviewsample/MainActivity.kt)

### IP Validation View
- Add `IPValidationView` to your xml layout.
```xml
        <in.technowolf.ipvalidationview.IpValidationView
            android:id="@+id/ipValidationView"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"/>
```
* Note: To prevent device keyboard cover the validation view, Add the view inside 
  your TextInputLayout * 
  
```xml
    <!--Text input layout-->
    <com.google.android.material.textfield.TextInputLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:hint="IP Address">
        <!--Edit text-->
        <com.google.android.material.textfield.TextInputEditText
            android:id="@+id/etIpAddress"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_margin="20dp"/>
        <!--IpValidationView-->
        <in.technowolf.ipvalidationview.IpValidationView
            android:id="@+id/ipValidationView"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"/>
    </com.google.android.material.textfield.TextInputLayout>
```

### Initialize the view
```kotlin
    val ipEditText = findViewById<EditText>(R.id.etIpAddress)
    val ipValidationView = findViewById<IpValidationView>(R.id.ipValidationView)
    
    // Edit text instance for view
    ipValidationView.ipEditText = passEditText
    // Activation color when IP is valid
    ipValidationView.enabledColor = android.R.color.holo_orange_dark
```

### Checking if IP is valid
```kotlin
    // Returns true if IP is valid, False if not.
    val boolean = ipValidationView.isIpValid
```

## License

[MIT License](LICENSE) Copyright (c) 2022 TechnoWolf FOSS

IP Validation View is provided under terms of MIT license.

## Links

[Issue Tracker](https://gitlab.com/technowolf/ip-validation-view/-/issues)
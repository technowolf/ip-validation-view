package `in`.technowolf.ipvalidationviewsample

import `in`.technowolf.ipvalidationview.IpValidationView
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val ipEditText = findViewById<EditText>(R.id.etIpAddress)
        val ipValidationView = findViewById<IpValidationView>(R.id.ivv)

        ipValidationView.ipEditText = ipEditText
        ipValidationView.enabledColor = android.R.color.holo_orange_dark

        ipValidationView.setOnClickListener {
            Log.d("TAG", "onCreate: ${ipValidationView.isIpValid}")
        }

    }
}
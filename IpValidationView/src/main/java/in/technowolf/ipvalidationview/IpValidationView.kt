package `in`.technowolf.ipvalidationview

import `in`.technowolf.ipvalidationview.databinding.IpValidationViewBinding
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.Interpolator
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doAfterTextChanged
import kotlin.math.cos
import kotlin.math.pow

class IpValidationView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attributeSet, defStyle) {
    private var isDigit = false

    var enabledColor: Int = android.R.color.holo_blue_dark
    var isIpValid: Boolean = false
        private set

    /*view binding*/
    private val binding =
        IpValidationViewBinding.inflate(LayoutInflater.from(context), this, true)

    var ipEditText: EditText? = null
        set(value) {
            value?.addTextChangedListener {
                validateIp(it.toString())
            }
            value?.doAfterTextChanged {
                checkValidation(it.toString())
            }
            field = value
        }

    private fun validateIp(string: String) {
        if (string.isBlank() ||
            string.chars().anyMatch(Character::isDigit).not() ||
            string.matches(Regex(IP_REGEX)).not()
        ) {
            binding.apply {
                layoutDigits.digitsSymbol.setTextColor(
                    resources.getColor(R.color.gray, context.theme)
                )
                layoutDigits.digitsTv.setTextColor(
                    resources.getColor(R.color.gray, context.theme)
                )
                isDigit = false
            }
        }

        if (string.chars().anyMatch(Character::isDigit) &&
            string.matches(Regex(IP_REGEX))
        ) {
            binding.apply {
                layoutDigits.digitsSymbol.setTextColor(
                    resources.getColor(enabledColor, context.theme)
                )
                layoutDigits.digitsTv.setTextColor(
                    resources.getColor(enabledColor, context.theme)
                )
                if (isDigit.not()) {
                    animateSymbol(layoutDigits.digitsSymbol)
                    isDigit = true
                }
            }
        }
    }

    private fun animateSymbol(view: View) {
        val animation = AnimationUtils.loadAnimation(context, R.anim.bounce)
        animation.interpolator = BounceInterpolator()
        view.startAnimation(animation)
    }

    private fun checkValidation(string: String) {
        isIpValid = (string.isNotBlank() && isDigit)
    }

    companion object {
        const val IP_REGEX =
            "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\$"
    }
}

class BounceInterpolator(
    private val amplitude: Double = 1.0,
    private val frequency: Double = 10.0
) : Interpolator {

    override fun getInterpolation(time: Float): Float {
        return (-1 * Math.E.pow(-time / amplitude) * cos(frequency * time) + 1).toFloat()
    }
}
